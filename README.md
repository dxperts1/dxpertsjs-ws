# Dxperts websocket interface (dxpertsjs-ws)

Pure JavaScript Dxperts websocket library for node.js and browsers. Can be used to easily connect to and obtain data from the Dxperts blockchain via public apis or local nodes.

Credit for the original implementation goes to [jcalfeee](https://github.com/jcalfee).

[![npm version](https://img.shields.io/npm/v/dxpertsjs-ws.svg?style=flat-square)](https://www.npmjs.com/package/dxpertsjs-ws)
[![npm downloads](https://img.shields.io/npm/dm/dxpertsjs-ws.svg?style=flat-square)](https://www.npmjs.com/package/dxpertsjs-ws)

## Setup

This library can be obtained through npm:

```
npm install dxpertsjs-ws
```

## Usage

Several examples are available in the /examples folder, and the tests in /test also show how to use the library.

Browser bundles are provided in /build/, for testing purposes you can access this from rawgit:

```
<script type="text/javascript" src="https://cdn.rawgit.com/dxperts/dxpertsjs-ws/build/dxpertsjs-ws.js" />
```

A variable dxperts_ws will be available in window.

For use in a webpack/browserify context, see the example below for how to open a websocket connection to the gvxlive API and subscribe to any object updates:

```
var {Apis} = require("dxpertsjs-ws");
Apis.instance("wss://dxperts.gvxlive.io/ws", true).init_promise.then((res) => {
    console.log("connected to:", res[0].network);
    Apis.db.set_subscribe_callback( updateListener, true )
});

function updateListener(object) {
    console.log("set_subscribe_callback:\n", object);
}
```

The `set_subscribe_callback` callback (updateListener) will be called whenever an object on the blockchain changes or is removed. This is very powerful and can be used to listen to updates for specific accounts, assets or most anything else, as all state changes happen through object updates. Be aware though that you will receive quite a lot of data this way.

# Blockproducer node endpoints

This is a non-exhaustive list of endpoints available from the blockproducer_node executable, which provides the API server of Dxperts.

## Public API

Please see all available methods in the [official documentation](https://dev.dxperts.network/en/master/api/blockchain_api.html).

### Database API

To access the [Database API](https://dev.dxperts.network/en/master/api/blockchain_api/database.html), you can use the `Apis.db` object.

**Usage example**
`Apis.db.get_objects(["1.3.0", "2.0.0", "2.1.0"])`

### History API

To access the [Account History API](https://dev.dxperts.network/en/master/api/blockchain_api/history.html), you can use the `Apis.history` object.

**Usage example**
`Apis.history.get_account_history("1.2.849826", "1.11.0", 10, "1.11.0")`

## Tests

The tests show several use cases, to run, simply type `npm run test`. The tests require a local blockproducer node to be running, as well as an active internet connection.
